const Discord = require("discord.js");
const client = new Discord.Client();
const fs = require('fs');

function getAutoBanList(type = 'id') {
  return fs.readFileSync('./list.txt')
    .toString()
    .split('\n')
    .map(person => person.split(': ')
      [type === 'id' ? 1 : 0]);
}


client.on("ready", () => {
  console.info("Cyborg: ENGAGE");
  console.info('Users banned:', getAutoBanList('user')
    .join(', '));
});

client.on("guildMemberAdd", (member) => {
  const list = getAutoBanList();
  if (list.includes(member.id)) {
    member.ban({ days: 0, reason: 'Autoban' });
    console.info('User Banned:', member.user.username, 'at', Date())
  }
});

client.login("NzgwODgyNDg5NTkyNzc0NjU3.X71jfQ.6uawEQxflOlyfnD6lbk0L7DTRvg");